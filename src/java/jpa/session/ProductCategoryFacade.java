/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import entities.ProductCategory;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author 98038064
 */
@Stateless
public class ProductCategoryFacade extends AbstractFacade<ProductCategory> {

    @PersistenceContext(unitName = "F16_csd322_LorisRugolo_Lab3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductCategoryFacade() {
        super(ProductCategory.class);
    }
    
}
